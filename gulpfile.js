var gulp = require('gulp'), // Gulp JS
    less = require('gulp-less'), // Плагин для less
    autoprefixer = require('gulp-autoprefixer'), // Подключаем библиотеку для авто добавления префиксов
    csso = require('gulp-csso'), // Минификация CSS
    uncss = require('gulp-uncss'),// проверяет html и удаляет неиспользуемые стили css
    imagemin = require('gulp-imagemin'), // Минификация изображений
    pngquant = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
    cache = require('gulp-cache'), // Подключаем библиотеку кеширования для работы с изображениями
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    del = require('del'), // Библиотека для удаления файлов
    browserify = require('gulp-browserify'), // Browserify
    browserSync = require('browser-sync'), // Browser-sync
    jshint = require('gulp-jshint'), // JS Hint
    stylish = require('jshint-stylish'), // JS Hint external reporter
    stripDebug = require('gulp-strip-debug'), // Removing console.logs
    preprocess = require('gulp-preprocess'), // Препроцессит на основе значения переменной Environment
    env = require('gulp-env'), // в зависимости от типа сборки дает значение переменной Environment
    vfb = require('vinyl-ftp-branch'),
    ftp = require('vinyl-ftp'); // плагины для загрузки на ftp

// Удаление папки перед сборкой
gulp.task('clean', function() {
    return del.sync('./dev-build/'); // Удаляем папку перед сборкой
});

// Задаем значение Environment
gulp.task('env-development', function() {
    return process.env.NODE_ENV = 'development';
});
gulp.task('env-production', function() {
    return process.env.NODE_ENV = 'production';
});

// Собираем Less
gulp.task('less', function() {
    gulp.src(['./src/less/main.less', './src/less/**/*.less'])
        .pipe(concat('style.less'))
        .pipe(less('style.less')) // собираем less
    .on('error', console.log) // Если есть ошибки, выводим и продолжаем
    .pipe(autoprefixer(['last 5 versions'], { cascade: true })) // Создаем префиксы
    .pipe(gulp.dest('./dev-build/css/')) // записываем css
    .pipe(browserSync.reload({stream: true})); // даем команду на перезагрузку css
});

// Собираем css библиотеки
gulp.task('css', function() {
    gulp.src('./src/lib/*.css')
        .pipe(gulp.dest('./dev-build/css/'));
});

// Собираем HTML
gulp.task('html', function () {
  gulp.src('./src/*.html')
    .pipe(preprocess()) // препроцессим на основе значения NODE_ENV
    .pipe(gulp.dest('./dev-build/')) // записываем html
    .pipe(browserSync.reload({stream: true}));
});

// Собираем шрифты
gulp.task('fonts', function () {
  gulp.src('./src/fonts/**')
    .pipe(gulp.dest('./dev-build/fonts/')); // записываем fonts
});

// Собираем JS
gulp.task('js', function() {
    gulp.src(['./src/js/main.js'])
        .pipe(browserify()) // browserifyjs.org
        .pipe(concat('script.js')) // создаем script.js
        // .pipe(jshint()) // проверяем скрипт на ошибки
        .pipe(jshint.reporter('jshint-stylish', {beep: true})) // при ошибке пикает
        .pipe(gulp.dest('./dev-build/js'))
        .pipe(browserSync.reload({stream: true})); // даем команду на перезагрузку страницы
});

// Копируем и минимизируем изображения
gulp.task('images', function() {
    return gulp.src('./src/img/**/*')
        .pipe(cache(imagemin({ // Сжимаем с наилучшими настройками используя кэш
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('./dev-build/img'))
        .pipe(browserSync.reload({stream: true}));
});

// Создаем таск browser-sync
gulp.task('browser-sync', function() {
    browserSync({ // Выполняем browser Sync
        server: { // Определяем параметры сервера
            baseDir: 'dev-build' // Директория по умолчанию для сервера
        },
        notify: false // Отключаем уведомления
    });
});

// Очистка кэша (если проблемы с изображениями или другими кэш файлами)
gulp.task('clear', function () {
    return cache.clearAll();
});


// ЗАПУСК СЕРВЕРА РАЗРАБОТКИ gulp watch
gulp.task('watch', ['clean', 'env-development', 'browser-sync', 'less', 'css', 'html', 'fonts', 'images', 'js'], function() {
    // Предварительная сборка проекта

    // Отслеживание изменений и обновление в браузере
    gulp.watch('src/less/**/*.less', ['less']);
    gulp.watch('src/*.html', ['html']);
    gulp.watch('src/img/**/*', ['images']);
    gulp.watch('src/js/**/*', ['js']);
});


// СБОРКА ПРОЕКТА
gulp.task('build', ['env-production'], function() {
    // cleaning
    del.sync('./build/');

    // less
    gulp.src('./src/less/**/*.less')
        .pipe(concat('style.less'))
        .pipe(less('style.less')) // собираем less
        // .pipe(uncss({ // Убираем неиспользуемые стили
        //   html: [
        //     'src/index.html',
        //     'src/**/*.html'
        //     // './*.html'
        //     // 'http://localhost:4000/foundation/',
        //     // 'http://localhost:4000/budgets/'
        //   ]
        // }))
        .pipe(autoprefixer(['last 5 versions'], { cascade: true })) // Создаем префиксы
        .pipe(csso()) // минимизируем css
    .pipe(gulp.dest('./build/css/')); // записываем css

    // Убираем неиспользуемые стили
    gulp.src([
      'node_modules/bootstrap/dist/css/bootstrap.min.css'
    ])
        .pipe(uncss({
          html: [
            'src/index.html',
            'src/**/*.html'
            // './*.html'
            // 'http://localhost:4000/test/'
          ]
        }))
    .pipe(csso())
    .pipe(gulp.dest('build/css/'));

    // html
    gulp.src('./src/*.html')
        .pipe(preprocess()) // препроцессим
        .pipe(gulp.dest('./build/')); // записываем html

    // fonts
    gulp.src('./src/fonts/**')
        .pipe(gulp.dest('./build/fonts/')); // записываем fonts

    // js
    gulp.src(['./src/js/main.js'])
        .pipe(jshint()) // проверяем скрипт на ошибки
        .pipe(jshint.reporter('jshint-stylish', {beep: true})) // при ошибке пикает
        // .pipe(stripDebug()) // remove con.log
        .pipe(browserify()) // browserifyjs.org
        .pipe(concat('script.js'))
        // .pipe(uglify())
        .pipe(gulp.dest('./build/js'));

    // image
    gulp.src('./src/img/**/*')
        .pipe(imagemin({ // Сжимаем с наилучшими настройками
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('./build/img'));
});


// ЗАГРУЗКА НА FTP
gulp.task('ftp-push', function() {
    var options = vfb({
        host: 'university.netology.ru',
        port: '21',
        userKeyFile: '.ftpauth',
        userKey: 'netology',
        remotePath: '/JS/diploma',
        log: false
    });
    var conn = ftp.create(options);

    return gulp.src(['build/**'], {buffer: false})
            .pipe(conn.newer(options.remotePath))
            .pipe(conn.dest(options.remotePath));
});


gulp.task('default', ['watch']);