module.exports = function ($scope, $http) {
    function getParameterByName(name) {
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search); // раскладываем URL после знака ? на компоненты
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }

    var id = getParameterByName('id') || 'fbb-0001'; // если не задан id в адресе, то даем по умолчанию fbb-0001
    $scope.id = id;
    console.info('id =', id, 'getParam =', getParameterByName('id'));

    $http.get('https://netology-fbb-store-api.herokuapp.com/book/' + id).success(function(data) {
        $scope.book = data;
        console.log('Books data ok', $scope.book);
    });

    $http.get('https://netology-fbb-store-api.herokuapp.com/order/delivery').success(function(data) {
        $scope.delivery = data;
        console.log('Delivery ok', $scope.delivery);
    });

    $http.get('https://netology-fbb-store-api.herokuapp.com/order/payment').success(function(data) {
        $scope.payment = data;
        console.log('Payment ok', $scope.payment);
    });

    $http.get('https://netology-fbb-store-api.herokuapp.com/currency/').success(function(data) {
        $scope.currencies = data;
        $scope.totalprice = $scope.book.price; // итоговая цена пока равна только стоимости книги
        data.forEach(function(item, index){
            if (item.CharCode === "USD") { // Ищем валюту, в которй заданы цены на сайте по умолчанию
                $scope.defaultCurrency = item;
                console.log('Default currency FOUND');
            }
        });
        console.log('Currency OK', $scope.currencies);
        $scope.currencySelector = $scope.defaultCurrency; // задаем значение селекта по умолчанию
    });


    $scope.updatePaymentScope = function (method) {
        $scope.orderCtrl.payment = undefined; // сбрасываем значение способа оплаты
        $scope.needAddress = method.needAdress; // проверяем нужен ли адрес доставки
        $scope.totalprice = $scope.book.price; // сбрасываем итогову сумму с доставкой
        $scope.totalprice += method.price; // считаем итоговую сумму с доставкой
        console.log('needAddress =', $scope.needAddress);
        $http.get('https://netology-fbb-store-api.herokuapp.com/order/delivery/'+$scope.delivery.id+'/payment').success(function(data) {
            $scope.payment = data; // обновляем список доступных оплат для выбранной доставки
        });
        $scope.calculate();
    };

    $scope.purchase = function (form) {
        if (form.$valid) {
            $scope.order = {
                payment: {},
                delivery: {}
            };
            $scope.order.manager = "tox@tut.by";
            $scope.order.book = $scope.id;
            $scope.order.name = $scope.orderCtrl.name;
            $scope.order.phone = $scope.orderCtrl.phone;
            $scope.order.email = $scope.orderCtrl.email;
            $scope.order.comment = $scope.orderCtrl.comment || 'not set';
            $scope.order.delivery.id = $scope.delivery.id;
            $scope.order.delivery.address = $scope.orderCtrl.address || 'not set';
            $scope.order.payment.id = $scope.orderCtrl.payment;
            $scope.order.payment.currency = $scope.defaultCurrency.ID; // USD
            $scope.requestSent = false;
            $scope.requestError = false;

            $http.post('https://netology-fbb-store-api.herokuapp.com/order', $scope.order)
                        .success(function (data, status, headers, config) {
                            console.log("Данные отправлены на сервер", data, status);
                            $scope.requestSent = true;

                            return setTimeout(function(){document.location.href = './index.html';}, 7000);
                        })
                        .error(function (data, status, header, config) {
                            console.log("Ошибка отправки данных", data, 'status =', status);
                            if (status === 500) { // Т.к. это проблема бэкенда и не зависит от нас в нашем задании, то притворяемся будто заказ отправлен
                                $scope.requestSent = true;
                                return setTimeout(function(){document.location.href = './index.html';}, 7000);
                            } else {
                                $scope.requestError = true;

                                return form.$submitted = false;
                            }
                        });
        } else {
            alert('Пожалуйста, проверьте правильность заполнения необходимых полей, а также укажите способы доставки и оплаты');
        }
    };


    function convert(currFrom, priceFrom, currTo) {
        return (priceFrom * (currFrom.Value / currFrom.Nominal) / (currTo.Value / currFrom.Nominal)).toFixed(0);
    }

    $scope.calculate = function(){
        var curfrom = $scope.defaultCurrency;
        $scope.total = convert($scope.defaultCurrency, $scope.totalprice, $scope.currencySelector);
        console.log('calculate = ', $scope.total);

        return $scope.total;
    };
};