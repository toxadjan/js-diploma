module.exports = function($scope, $http) {
    function getParameterByName(name) {
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search); // раскладываем URL после знака ? на компоненты
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }

    var id = getParameterByName('id') || 'fbb-0001'; // если не задан id в адресе, то даем по умолчанию fbb-0001
    $scope.id = id;
    console.info('id =', id, 'getParam =', getParameterByName('id'));

    $http.get('https://netology-fbb-store-api.herokuapp.com/book/' + id).success(function(data) {
        $scope.book = data;
        console.log('AJAX ok', $scope.book);
    });

};