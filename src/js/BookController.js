module.exports = function ($scope, $http, $location) {
    $http.get('https://netology-fbb-store-api.herokuapp.com/book/').success(function(data) {
        $scope.books = data;
        $scope.maxItems = 4;
        console.log('AJAX ok');

        function getParameterByName(name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }
        console.log('searchParameter = ', getParameterByName('search'));

        if (getParameterByName('search')) {
            $scope.query = getParameterByName('search');
            $scope.maxItems = $scope.books.length; // при поиске ищем сразу по всей коллекции
        }
    });
};