require('angular');
require('angular-sanitize');

var BookController = require('./BookController'),
    SearchController = require('./SearchController'),
    BookDetailsController = require('./BookDetailsController'),
    BookOrderController = require('./BookOrderController'),
    MovingEyeController = require ('./MovingEyeController.js');

(function() {
    var app = angular.module('bookApp', ['ngSanitize', 'ng-sortable']);
    app.controller('BookController', ['$scope', '$http', BookController]);
    app.controller('SearchController', ['$scope', SearchController]);
    app.controller('BookDetailsController', ['$scope', '$http', BookDetailsController]);
    app.controller('BookOrderController', ['$scope', '$http', BookOrderController]);
    app.controller('MovingEyeController', ['$scope', MovingEyeController]);
    app.directive('orderForm', function() {
        return {
            restrict: 'E',
            templateUrl: 'order-form.html'
        };
    });
    app.directive('movingEye', function() {
        return {
            restrict: 'E',
            templateUrl: 'moving-eye.html',
            controller: 'MovingEyeController'
        };
    });
})();