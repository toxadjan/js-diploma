module.exports = function ($scope) {

    var $ = require('jquery');

    /* ---------------------------------------------- /*
     * EYE
    /* ---------------------------------------------- */
    $(document).on("mousemove touchmove", function(event) {

        // вычисляем коэффициенты отношениия отрезков по осям Х и Y (длину от цетра окружности до курсора к отрезку от 0 до центра окружности). 
        // затем вычисляем длину отрезка (на который надо будет сдвинуть зрачок) внутри круга по каждой оси на основании этих отношений.
        // в зависимости от положения курсора относительно центра окружности либо прибавляем к радиусу длину полученного отрезка по оси Y или Y, 
        // либо отнимаем отрезок от радиуса

        var centerX = $('.eye').offset().left + ($('.eye').width() / 2), // центр глаза по оси Х
            centerY = $('.eye').offset().top + ($('.eye').height() / 2), // центр глаза по оси Y
            R = ($('.eye').width() - $('.pupil').width()) / 2, // радиус глаза
            mouseX = event.pageX, // координаты мыши
            mouseY = event.pageY,
            rangeX = Math.abs(mouseX - centerX), // длина отрезка по оси Х от центра окружности до курсора
            rangeY = Math.abs(mouseY - centerY), // длина отрезка по оси Y от центра окружности до курсора
            ratioX = rangeX / centerX, // отношение длины отрезка rangeX к длине отрезка от 0 до центра окружности по X
            ratioY = rangeY / centerY, // отношение длины отрезка rangeY к длине отрезка от 0 до центра окружности по Y
            moveX = R * ratioX, // длины отрезков от цетра окружности до новой точки относительно отрезка от начала координат до мыши
            moveY = R * ratioY, // в соответствии к отношению длины отрезка rangeX к длине отрезка от 0 до центра окружности
            top, left; // отступы от нуля в блоке глаза до новой точки центра зрачка

        // console.log('mX =', mouseX, 'mY=', mouseY, 'cX =', centerX, 'cY =', centerY, 'R =', R);
        // console.log('rangeX=', rangeX, 'rangeY=', rangeY, 'ratioX=', ratioX, 'ratioY=', ratioY);
        // console.info('moveX=', moveX, 'moveY=', moveY);

        if (mouseX > centerX && mouseY > centerY) {
            left = R + moveX;
            top = R + moveY;

            return $('.pupil').css({
                top: top,
                left: left
            });
        }

        if (mouseX > centerX && mouseY < centerY) {
            left = R + moveX;
            top = R - moveY;

            return $('.pupil').css({
                top: top,
                left: left
            });
        }

        if (mouseX < centerX && mouseY > centerY) {
            left = R - moveX;
            top = R + moveY;

            return $('.pupil').css({
                top: top,
                left: left
            });
        }

        if (mouseX < centerX && mouseY < centerY) {
            left = R - moveX;
            top = R - moveY;
            return $('.pupil').css({
                top: top,
                left: left
            });
        }
    });
};